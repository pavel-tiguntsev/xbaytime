import Vue from 'vue';
import Vuex from 'vuex';
import dateFns from 'date-fns';

Vue.use(Vuex);

const actions = {
  initWeek({ commit }) {
    const startDate = dateFns.startOfWeek(new Date());
    for (let i = 0; i < 7; i += 1) {
      const day = dateFns.addDays(startDate, i);
      commit('PUSH_DAY', {
        wday: day.toString().slice(0, 3),
        date: dateFns.getDate(day),
      });
    }
  },
  startCountDown({ commit }) {
    commit('SET_TIMER_ACTIVE');
  },
  stopCountDown({ commit }) {
    commit('DEACTIVATE_TIMER');
  },
  addTask({ commit }, task) {
    commit('PUSH_NEW_TASK', task);
  },
};

const mutations = {
  PUSH_DAY(state, day) {
    state.week.push(day);
  },
  SET_TIMER_ACTIVE(state) {
    state.timerActive = true;
  },
  DEACTIVATE_TIMER(state) {
    state.timerActive = false;
  },
  PUSH_NEW_TASK(state, task) {
    state.testTasks.push({
      context: task,
      completed: 0,
    });
  },
};

export default new Vuex.Store({
  state: {
    week: [],
    isCountDown: true,
    timerActive: false,
    cursorDay: 0,
    dayTask: [
      { day: 0, tasks: '' },
    ],
    // eslint-disable-next-line
    testTasks: [{"context":"Enable force finish","completed":0},{"context":"Complete time tracker mode","completed":0},{"context":"Enable day switch","completed":0},{"context":"Make task list draggable","completed":0}],
    theme: {
      color: 'black',
    },
  },
  actions,
  mutations,
});
